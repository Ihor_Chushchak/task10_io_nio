package com.epam.implementation;

import java.io.InputStream;
import java.io.PushbackInputStream;

public class ImplementationUtils extends PushbackInputStream {
    public ImplementationUtils(InputStream in, int size) {
        super(in, size);
    }

    public ImplementationUtils(InputStream in) {
        super(in);
    }
}
