package com.epam.comments;

import com.epam.Functional;
import com.epam.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public final class RunComments {
    private static final Scanner INPUT = new Scanner(System.in);
    private static final File DEFAULT_FILE_NAME = new File(
            "D:\\ JavaApp\\task10_IONIO\\resource\\comments\\*.txt");
    private static final String LINE_COMMENT = "//";


    private static final Logger LOG = LogManager.getLogger(RunComments.class);
    private static File mainFile = new File(String.valueOf(DEFAULT_FILE_NAME));

    /**
     * isComment Slash - це коментар типу //.
     * */
    private static boolean isCommentSlash = true;


    /**
     * Заготовлені файли з java кодом.
     */
    private static File[] files;


    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;


    /**
     * Конструктор без параметрів, з ініцілізацією об'єктів.
     */
    private RunComments() {
        files = mainFile.getParentFile().listFiles();
        initMenu();
    }


    /**
     * Винесено окремим методом оскільки справа значення
     * будуть мінятись щоразу при воводі меню.
     */
    private void dynamicChangeMenu() {
        menu.put("1", " 1 - Change file, current file: "
                + mainFile.getName() + ".");
        menu.put("2", " 2 - Change display // comments: "
                + isCommentSlash + ".");
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        dynamicChangeMenu();
        menu.put("3", " 3 - Start reading from file.");
        menu.put("4", " 4 - Back to task menu.");

        methodsMenu.put("1", this::changeFile);
        methodsMenu.put("2", this::changeCommentSlash);
        methodsMenu.put("3", this::beginRead);
        methodsMenu.put("4", Menu::runMenu);

    }


    private void changeCommentSlash() {
        isCommentSlash = !isCommentSlash;
    }


    private void outputMenu() {
        dynamicChangeMenu();
        System.out.println("\nMENU DISPLAY COMMENTS:");
        menu.values().forEach(System.out::println);
    }

    private void showMenuSetting() {
        int size = files != null ? files.length : 0;
        System.out.println("\nMENU SETTING FILE:");
        for (int i = 0; i < size; i++) {
            System.out.println(" " + (i + 1) + " - Set file "
                    + files[i].getName() + ".");
        }
    }

    private void setFile(int index) {
        int size = files.length;
        //бо індекси на одиницю менше від меню вибору
        index--;
        for (int i = 0; i < size; i++) {
            if (index == i) {
                mainFile = files[i];
                if (LOG.isTraceEnabled()) {
                    LOG.trace("File successfully set!");
                }
                return;
            }
        }
        if (LOG.isTraceEnabled()) {
            LOG.trace("Can't set file!");
        }
    }

    private void changeFile() {
        int value;
        while (true) {
            try {
                System.out.println("\n");
                showMenuSetting();
                if (LOG.isInfoEnabled()) {
                    LOG.info("Please, select menu point.");
                }
                String inputInt = INPUT.nextLine();
                value = Integer.parseInt(inputInt);
                setFile(value);

                break;
            } catch (IllegalArgumentException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Size can't be less then zero");
                }
            } catch (Exception e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Please try again!\n");
                }
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Chose file(index): " + value);
        }
    }

    private void beginRead() throws IOException {
        if (isCommentSlash) {
            readSlashComment();
        }
    }


    private void readSlashComment() throws IOException {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(mainFile),
                        StandardCharsets.UTF_8))) {

            StringBuilder st = new StringBuilder();
            while (br.read() != -1) {
                String line = br.readLine();
                if (line != null) {
                    if (line.trim().contains(LINE_COMMENT)) {
                        line = deleteFirstSymbolsBeforeComment(line, LINE_COMMENT);
                        st.append(line.trim())
                                .append("\n");
                    }
                }
            }
            if (LOG.isDebugEnabled()) {
                LOG.info(st);
            }
        }
    }

    private String deleteFirstSymbolsBeforeComment(String line, String comment) {
        int size = line.length();
        while (!line.startsWith(comment)) {
            //delete first symbol, coz comment can't be after words
            line = line.substring(1, size);
            size--;
        }
        return line;
    }


    public static void run() {
        String key;
        RunComments menu = new RunComments();
        while (true) {
            try {
                System.out.println("\n");
                menu.outputMenu();
                if (LOG.isInfoEnabled()) {
                    LOG.info("Please, select menu point.");
                }
                key = INPUT.nextLine();
                menu.methodsMenu.get(key).start();
            } catch (NullPointerException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nError\nPlease choose correct point.");
                }
            } catch (IOException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nFile error!");
                }
            } catch (Exception e) {
                if (LOG.isFatalEnabled()) {
                    LOG.fatal("\nFatal error");
                }
            }
        }
    }
}
