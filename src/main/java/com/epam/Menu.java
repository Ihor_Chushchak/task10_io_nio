package com.epam;

import com.epam.bufferclass.RunBufferClass;
import com.epam.comments.RunComments;
import com.epam.compare.CompareUtils;
import com.epam.displaycontent.RunDisplayContent;
import com.epam.displaycontent.Runn;
import com.epam.implementation.RunImplementations;
import com.epam.serializable.RunSerializable;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public final class Menu {
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;
    private static final Scanner INPUT = new Scanner(System.in);
    private static final Logger LOG = LogManager.getLogger(Menu.class);

    private Menu() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - (task 1)Serializable.");
        menu.put("2", " 2 - (task 2)Compare reading and writing files "
                + "using usual and buffer reader for different sizes.");
        menu.put("3", " 3 - (task 3)Own InputStream example.");
        menu.put("4", " 4 - (task 4)Read a Java source code file.");
        menu.put("5", " 5 - (task 5)Get contents directories.");
        menu.put("6", " 6 - (task 6)Own SomeBuffer class example.");
        menu.put("7", " 7 - (task 7)NIO example.");
        menu.put("0", " 0 - Exit.");

        methodsMenu.put("1", RunSerializable::runTask1);
        methodsMenu.put("2", CompareUtils::run);
        methodsMenu.put("3", RunImplementations::run);
        methodsMenu.put("4", RunComments::run);
        methodsMenu.put("5", RunDisplayContent::run);
        methodsMenu.put("6", RunBufferClass::run);
//        methodsMenu.put("7", Server::runNIO);


        methodsMenu.put("0", this::exitFromProgram);

    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    private void exitFromProgram() {
        System.exit(0);
    }

    public static void runMenu() {
        String key;
        Menu menu = new Menu();
        while (true) {
            try {
                System.out.println("\n");
                menu.outputMenu();
                LOG.info("Please, select menu point.");
                key = INPUT.nextLine();
                System.out.println("\n\n");
                menu.methodsMenu.get(key).start();
            } catch (NullPointerException e) {
                System.out.println(e);
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nChoose correct choice please.");
                }
            } catch (IOException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nSomething wrong with files or Serializable.");
                }
            } catch (ClassNotFoundException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nCan't find class.");
                }
            } catch (Exception e){
                System.out.println(e);
                if (LOG.isFatalEnabled()) {
                    LOG.fatal("Main crushed!");
                }
            }
        }
    }
}
