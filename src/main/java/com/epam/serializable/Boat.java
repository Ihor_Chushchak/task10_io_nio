package com.epam.serializable;

import java.io.Serializable;

public class Boat implements Serializable {
    private static transient String defaultShipName = "BOARD_001";
    private transient int defaultHealthPoint = 55;
    private static final String defaultUnitName = "UNIT_ALPHA";
    private final int[] size = {1,2,3};

    @Override
    public String toString() {
        return "Boat{" +
                "Boat name is (static transient) - " + defaultShipName + '\'' +
                ", it has " + defaultHealthPoint + " health points (just transient)" +
                ", its unit name is (final transient) - " + defaultUnitName + '\'' +
                '}';
    }
}
