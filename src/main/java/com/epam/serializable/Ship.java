package com.epam.serializable;

import java.io.Serializable;

public class Ship implements Serializable {
    private String shipName;
    private int healthPoint;
    private transient String unitName ;
    private final int[] size = {1,2,3};

    public Ship(String shipName, int healthPoint, String unitName) {
        this.shipName = shipName;
        this.healthPoint = healthPoint;
        this.unitName = unitName;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "Ship name is - " + shipName + '\'' +
                ", it has " + healthPoint + " health points" +
                ", its unit name is (transient) - " + unitName + '\'' +
                '}';
    }
}


