package com.epam.serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class RunSerializable implements AutoCloseable {

    private static final Logger logger = LogManager.getLogger(RunSerializable.class);
    private static final String DEFAULT_PATH_FILES
            = "D:\\JavaApp\\task10_IONIO\\resource\\Serializable\\";
    private static final int MAGIC_HEALTH_POINT = 33;

    private static void serializableInFile(
            final String fileName,
            final Object obj) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(obj);
            oos.flush();
        }
    }

    private static Object serializableFromFile(final String fileName)
            throws IOException, ClassNotFoundException {
        try (FileInputStream fin = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(fin)) {
            return ois.readObject();
        }

    }

    public static void runTask1() throws IOException, ClassNotFoundException {
        final String shipFile = DEFAULT_PATH_FILES
                + "serializableShip.txt";
        final String boatFile = DEFAULT_PATH_FILES
                + "serializableBoat.txt";

        Ship ship = new Ship(
                "BOARD_007",
                MAGIC_HEALTH_POINT,
                "Vol");
        Boat boat = new Boat();

        if (logger.isTraceEnabled()) {
            logger.trace("Ship before: " + ship);
            logger.trace("Boat before: " + boat);
        }
        if (logger.isInfoEnabled()) {
            //статичні і файнал поля не можна серіалізувати
            logger.info("In Ship transient is: shipName");
            logger.info("In Boat transient is: health point\n");
        }

        serializableInFile(shipFile, ship);
        Ship ship1 = (Ship) serializableFromFile(shipFile);


        serializableInFile(boatFile, boat);
        Boat boat1 = (Boat) serializableFromFile(boatFile);

        if (logger.isInfoEnabled()) {
            logger.info("Object father(after): " + ship1);
            logger.info("Object sun(after): " + boat1);
        }
    }

    @Override
    public void close() {
        if (logger.isInfoEnabled()) {
            logger.info("Closed files");
        }
    }
}
