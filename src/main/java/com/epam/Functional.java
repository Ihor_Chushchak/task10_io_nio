package com.epam;


@FunctionalInterface
public interface Functional {
    void start() throws Exception;
}
