package com.epam.compare;

import com.epam.Functional;
import com.epam.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public final class RunCompare {
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;
    private static final Scanner INPUT = new Scanner(System.in);
    private static final Logger logger = LogManager.getLogger(RunCompare.class);

    private RunCompare() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - Set file.");
        menu.put("2", " 2 - Show name current filename.");
        menu.put("3", " 3 - Read by InputStream(without output content).");
        menu.put("4", " 4 - Read by BufferedReader in InputStream(without output content).");
        menu.put("5", " 5 - Read just BufferedReader(without output content).");
        menu.put("6", " 6 - Write by BufferedReader in InputStream to new created file.");
        menu.put("7", " 7 - Show contents current of file.");
        menu.put("8", " 8 - Back to task menu.");

        methodsMenu.put("1", CompareUtils::setFile);
        methodsMenu.put("2", CompareUtils::showNameCurrentFile);
        methodsMenu.put("3", CompareUtils::readByByte);
        methodsMenu.put("4", CompareUtils::readByBufferedReaderInInputStream);
        methodsMenu.put("5", CompareUtils::readByJustBufferedReader);
        methodsMenu.put("6", CompareUtils::writeByBufferedReaderInInputStream);
        methodsMenu.put("7", CompareUtils::showContentFile);
        methodsMenu.put("8", Menu::runMenu);
    }

    private void showMenu() {
        System.out.println("\nMENU READING:");
        menu.values().forEach(System.out::println);
    }

    static void runMenuBuffer() {
        String key;
        RunCompare runCompare = new RunCompare();
        while (true) {
            try {
                System.out.println("\n");
                runCompare.showMenu();
                if (logger.isInfoEnabled()) {
                    logger.info("Please, select menu point.");
                }
                key = INPUT.nextLine();
                runCompare.methodsMenu.get(key).start();
            } catch (NullPointerException e) {
                if (logger.isErrorEnabled()) {
                    logger.error("\nError\nPlease choose correct point.");
                }
//            } catch (IOException e) {
//                if (logger.isErrorEnabled()) {
//                    logger.error("\nFile error!");
//                }
//            } catch (Exception e) {
//                if (logger.isFatalEnabled()) {
//                    logger.fatal("\nFatal error.");
//                }
            } catch (Exception e) {
                logger.info(e);
            }
        }
    }
}
